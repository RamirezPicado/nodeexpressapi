const express = require ('express') 
const router = express.Router () 
const userController = require ('../ controllers / user.controllers'); 
// Recuperar todos los usuarios 
router.get ('/', userController.findAll); 
// Crear un nuevo usuario 
router.post ('/', userController.create); 
// Recuperar un solo usuario con id 
router.get ('/: id', userController.findOne); 
// Actualizar un usuario con id 
router.put ('/: id', userController.update); 
// Eliminar un usuario con id 
router.delete ('/: id', userController.delete); 
module.exports = router