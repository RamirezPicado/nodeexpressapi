const express = require ('express'); 
const bodyParser = require ('body-parser'); 
// crea la aplicación express 
const app = express (); 
// Configurar el puerto del servidor 
const port = process.env.PORT || 4000; 
// analizar solicitudes de tipo de contenido - application / x-www-form-urlencoded 
app.use (bodyParser.urlencoded ({extended: true})) 
// analizar solicitudes de tipo de contenido - application / json 
app.use (bodyParser .json ()) 
// Configuración de la base de datos 
const dbConfig = require ('./ config / db.config.js'); 
const mongoose = require ('mangosta'); 
mangosta.Promesa = global.Promesa; 
// Conexión a la base de datos 
mongoose.connect (dbConfig.url, { 
useNewUrlParser: true 
}). 
Then 
(() =>
{
  console.log ("Conectado exitosamente a la base de datos");
}). catch (err => { 
  console.log ('No se pudo conectar a la base de datos.', err); 
  process.exit (); 
}); 
// define una ruta raíz / predeterminada 
app.get ('/', (req, res) => { 
   res.json ({"message": "Hello World"}); 
}); 

const userRoutes = require ('./ src / routes / user.routes') 
// usar como middleware 
app.use ('/ api / users', userRoutes) 
// escucha las solicitudes 
app.listen (port, () => { 
   console.log (`El servidor de nodo está escuchando en el puerto $ {port}`); 
});