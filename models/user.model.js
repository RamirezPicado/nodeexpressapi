const mongoose = require ('mangosta'); 

const UserSchema = mongoose.Schema ({ 
    first_name: String, 
    last_name: String, 
    email: String, 
    phone: String, 
    is_active: {type: Boolean, default: false} , 
    is_verified: {type: Boolean, default: false} , 
    is_deleted: {tipo: booleano, predeterminado: falso} 
}, { 
   
    tiempo: verdadero 
}); 

module.exports = mongoose.model ('Usuario', UserSchema);